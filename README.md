# shopsync
Scripts to push and pull Shopify assets

## Why
Wanted to version control teh files

## How
* The Shopify HTTP API
* JavaScript so it might work on Windoze

Do install (directions below) then copy `etc/sample.env.js` to `etc/env.js` and put in your details.

To download your theme's files:
```
bin/pull [file1] [file2] [...]
```
To deploy your theme's files:
```
bin/push [file1] [file2] [...]
```
Omit arguments to push or pull the entire theme.

## Install
`git clone https://bitbucket.org/jessetane/shopsync <myshopname>`  
`cd <myshopname>`  
`npm install`  

## License
[WTFPL](http://www.wtfpl.net/txt/copying/)